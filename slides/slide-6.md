### Close the browser

**Selenium**

```java
if (driver != null) {
    driver.close();
}
```

**Selenide**

```java
// Nothing, Selenide automatically closes after test suite finishes.
```