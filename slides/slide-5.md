## Selenide vs Selenium Examples

### Creating a browser

**Selenium**

```java
DesiredCapabilities desiredCapabilities = DesiredCapabilities.htmlUnit();
desiredCapabilities.setCapability(HtmlUnitDriver.INVALIDSELECTIONERROR, true);
desiredCapabilities.setCapability(HtmlUnitDriver.INVALIDXPATHERROR, false);
desiredCapabilities.setJavascriptEnabled(true);
WebDriver driver = new HtmlUnitDriver(desiredCapabilities);
```

**Selenide**

```java
open("/my-application/login");
// Run with -Dbrowser=htmlunit, chrome, firefox, or ie
```