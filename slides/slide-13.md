### Screenshots

**Selenium**

```java
if (driver instanceof TakesScreenshot) {
  File scrFile = ((TakesScreenshot) webdriver).getScreenshotAs(OutputType.FILE);
  File targetFile = new File("c:\temp\" + fileName + ".png");
  FileUtils.copyFile(scrFile, targetFile);
}
```

**Selenide**

```java
takeScreenShot("my-test-case");
```

**JUnit Rules**

```java
public class MyTest {

  @Rule // automatically takes screenshot of every failed test
  public ScreenShooter makeScreenshotOnFailure = ScreenShooter.failedTests();
}
```