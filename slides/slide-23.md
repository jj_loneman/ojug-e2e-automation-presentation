## AutoIt Example Script

```vb
; The script needs admin rights to use ControlClick successfully
#RequireAdmin

; Option 2 allows substring matching for WinTitles
Opt ( "WinTitleMatchMode", 2 )

; Option 1 allows searching of child windows
Opt ( "WinSearchChildren", 1 )

;ConsoleWrite ( @CRLF & "hWnd: " & $hWnd & @CRLF )
;Sleep ( 500 )

For $i = 0 To 70
  ; Get the hWnd of the window
  Local $hWnd = WinActivate ( "Enter registration key" )
  Sleep ( 100 )

  ; If the button control was found, attempt to click it
  If ControlGetText ($hWnd, "Continue unregistered", "[TEXT:Continue unregistered]") = "Continue unregistered" Then
    ConsoleWrite( "== If Statement was True ==" & @CRLF )
    Local $x = ControlClick ( $hWnd, "Continue unregistered", "[TEXT:Continue unregistered]", "left", 1 )
    ConsoleWrite( "ControlClick success?: " & $x & @CRLF & @CRLF)
  Else
    ConsoleWrite ( "== If statement was false" & @CRLF )
  EndIf
  Sleep ( 5500 )
Next
```