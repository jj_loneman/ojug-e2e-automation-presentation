### Assert element text

**Selenium**

```java
assertEquals("Customer profile", driver.findElement(By.id("customerContainer")).getText());
```

**Selenide**

```java
$("#customerContainer").shouldHave(text("Customer profile"));
```