## Using Selenide

### build.gradle

```groovy
dependencies {
  testCompile 'com.codeborne:selenide:4.9.1'
}
```

### JUnit

```java
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

// ...

@Test
public void userCanLoginByUsername() {
   open("/login");
   $(By.name("username")).setValue("johny");
   $("#submit").click();
   $(".success-message").shouldHave(text("Hello, Johny!"));
}
```