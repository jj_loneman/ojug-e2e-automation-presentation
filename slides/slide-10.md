### Assert css class

**Selenium**

```java
try {
    WebElement element = driver.findElement(By.id("customerContainer"));
    fail("Element should not exist: " + element);
}
catch (WebDriverException itsOk) {}
```

**Selenide**

```java
$("#customerContainer").shouldNot(exist);
```