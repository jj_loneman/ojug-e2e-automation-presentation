## Katalon Stuio

* <https://www.katalon.com/>
* Full-fledged IDE
* Reusable page objects
* Integration with:
  * Git
  * JIRA, qTest
  * Jenkins, other CI tools