### Handling JavaScript Alerts

**Selenium**

```java
try {
  Alert alert = checkAlertMessage(expectedConfirmationText);
  alert.accept();
} catch (UnsupportedOperationException alertIsNotSupportedInHtmlUnit) {
  return;
}
Thread.sleep(200); // sometimes it will fail
```

**Selenide**

```java
confirm("Are you sure to delete your profile?");
```