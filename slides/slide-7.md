### Find element by id

**Selenium**

```java
WebElement customer = driver.findElement(By.id("customerContainer"));
```

**Selenide**

```java
WebElement customer = $("#customerContainer");
```

or

```java
WebElement customer = $(By.id("customerContainer"));
```