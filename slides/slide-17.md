## Advantages

* Very easy to use
* Easy to install

## Disadvantages

* No reusable objects
* No integration with Git or JIRA