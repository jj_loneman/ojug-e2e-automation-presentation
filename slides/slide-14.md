## Advantages

* Less boilerplate code
* Easier to write and manage tests

## Disadvantages

* Still requires manual Selenium driver management
* Tests can take a long time to write
  * This includes creating page objects
* Not easy for non-developers to use
  * Need good programming skills and experience to set up & integrate with other tools/frameworks
* Support can be slow from community