### Getting element from collection

**Selenium**

```java
WebElement element = driver.findElements(By.tagName("li")).get(5);
```

**Selenide**

```java
$("li", 5);
```