## Advantages

* Easy to use and manage
* Easy to install
* Easy for non-developers to use
* Integration with JIRA, Jenkins, Git
* Integration with common databases
* Decent support community

## Disadvantages

* Can be confusing at times
* Sometimes weird things can happen (make sure you use Git)
* Setting up database connectionos that aren't available out-of-the-box can be a pain